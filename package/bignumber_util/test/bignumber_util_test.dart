import 'package:flutter_test/flutter_test.dart';

import 'package:bignumber_util/bignumber_util.dart';

void main() {
  test('adds one to input values', () {
    final bignumberutil = Bignumber_util();
    expect(bignumberutil.Sum("9","5"), "14");
    expect(bignumberutil.Sum("91","5"), "96");
    expect(bignumberutil.Sum("900","5"), "905");
  });
}
